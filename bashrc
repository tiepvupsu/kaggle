alias kaggle_nb='docker run -p 4000:4000 -i -t -v `pwd`:/kaggle gcr.io/kaggle-images/python jupyter notebook --ip=0.0.0.0 --port=4000 --allow-root'

alias rebuild='cd ~/w/kaggle && python3 setup.py install && cd -'

