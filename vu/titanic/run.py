import os
from typing import Any, List, Tuple

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.compose import make_column_transformer
from sklearn.ensemble import RandomForestClassifier
from sklearn import impute
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline, make_union
from sklearn import preprocessing
import xgboost as xgb

from vu.base import feature

TRAIN_CSV = "../../data/titanic/train.csv"
TEST_CSV = "../../data/titanic/test.csv"
RESPONSE_COLUMN = "Survived"
VALIDATION_RATIO = 0.0


class FamilySizeAndIsAloneCreator(BaseEstimator, TransformerMixin):
    def fit(self, X=None, y=None):
        return self

    def transform(self, data):
        family_size = data["SibSp"] + data["Parch"] + 1
        is_alone = np.array(family_size == 1, np.int8)
        return np.stack([family_size, is_alone], axis=-1)


def preprocess() -> FeatureUnion:
    embarked_pipeline = make_pipeline(
        impute.SimpleImputer(strategy="most_frequent"),
        preprocessing.OneHotEncoder(sparse=False),
    )

    age_pipeline = make_pipeline(
        impute.SimpleImputer(strategy="median"),
        preprocessing.StandardScaler(),
        preprocessing.KBinsDiscretizer(
            n_bins=5, encode="ordinal", strategy="uniform"
        ),
    )
    column_transform = make_column_transformer(
        (feature.LabelEncoderHelper(), ["Sex"]),
        (embarked_pipeline, ["Embarked"]),
        (age_pipeline, ["Age", "Fare"]),
    )

    full_pipeline = make_union(FamilySizeAndIsAloneCreator(), column_transform)
    return full_pipeline


def get_models(testing: bool = False) -> List[Tuple[str, Any]]:
    clf = xgb.XGBClassifier()
    if testing:
        parameters = {
            "eta": [0.05],
            "max_depth": [3],
            "min_child_weight": [3],
            "gamma": [0.0],
            "colsample_bytree": [0.3],
        }
    else:
        parameters = {
            "eta": [0.05, 0.15, 0.25, 0.30],
            "max_depth": [3, 6, 10, 15, 20],
            "min_child_weight": [3, 7, 10],
            "gamma": [0.0, 0.1, 0.3, 0.4, 0.6, 1.0],
            "colsample_bytree": [0.3, 0.5, 1, 1.5],
        }
    xgb_model = GridSearchCV(
        clf, parameters, n_jobs=-1, scoring="accuracy", cv=10, verbose=1
    )

    # random forest
    rf = RandomForestClassifier()
    rf_parameters = {
        "n_estimators": [10, 20, 50, 100],
        "max_depth": [3, 5, 7, 10],
        "min_samples_split": [2, 3, 4, 5, 6],
        "min_samples_leaf": [1, 2],
    }
    rf_model = GridSearchCV(
        rf, rf_parameters, n_jobs=-1, scoring="accuracy", cv=10, verbose=1
    )
    return [("xgb", xgb_model), ("rf", rf_model)]


def run(testing: bool = False) -> None:
    np.random.seed(100)
    # load data
    train_df = pd.read_csv(TRAIN_CSV)
    test_df = pd.read_csv(TEST_CSV)

    print("============= train data info ================")
    print(train_df.info())
    print("============= test data info ================")
    print(test_df.info())

    train_label = train_df[RESPONSE_COLUMN].tolist()
    train_data = train_df.drop(columns=[RESPONSE_COLUMN])

    for model_name, model in get_models(testing=testing):
        pipeline = Pipeline(steps=[("prep", preprocess()), ("model", model)])
        # save to submission
        pipeline.fit(train_data, train_label)
        trained_model = pipeline["model"]
        trained_prep = pipeline["prep"]
        test_pred = trained_model.predict(trained_prep.transform(test_df))
        # submit
        test_df[RESPONSE_COLUMN] = test_pred
        submission_path = os.path.join("./submissions", model_name + ".csv")
        print(f"======================== {model_name} ======================")
        print("Best CV score: {:.5f}".format(trained_model.best_score_))
        print("Submission path", submission_path)
        test_df[["PassengerId", "Survived"]].to_csv(
            submission_path, index=False
        )


if __name__ == "__main__":
    run(testing=True)
