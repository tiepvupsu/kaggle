from typing import List

import numpy as np
import pandas as pd
from pandas.api.types import is_string_dtype
from sklearn import impute
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import preprocessing


class LabelEncoderHelper(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.encoder = preprocessing.LabelEncoder()

    def fit(self, x, y=None):
        self.encoder.fit(x)
        return self

    def transform(self, x, y=None):
        res = self.encoder.transform(x)
        if res.ndim == 1:
            res = res.reshape(-1, 1)
        return res


class BaseNoLearnTransform:
    """Base class for Transformation without learning"""

    def _transform(self, data: pd.DataFrame):
        assert False, "Must be implemented in subclasses"

    def fit_transform(self, data: pd.DataFrame) -> pd.DataFrame:
        return self._transform(data)

    def transform(self, data: pd.DataFrame) -> pd.DataFrame:
        return self._transform(data)


class DropColumns(BaseNoLearnTransform):
    def __init__(self, column_names: List[str]) -> None:
        self.column_names = column_names

    def _transform(self, data: pd.DataFrame) -> pd.DataFrame:
        return data.drop(columns=self.column_names)


class LabelEncoder:
    def __init__(self, column: str):
        self.column = column
        self.label_encoder = preprocessing.LabelEncoder()

    def fit_transform(self, data: pd.DataFrame) -> pd.DataFrame:
        assert self.column in data.columns
        encoded_column = self.label_encoder.fit_transform(data[self.column])
        data[self.column] = encoded_column
        return data

    def transform(self, data: pd.DataFrame) -> pd.DataFrame:
        assert self.column in data.columns
        encoded_column = self.label_encoder.transform(data[self.column])
        data[self.column] = encoded_column
        return data


class OnehotEncoder:
    """ Note: whilte df.get_dummies can do onehot encoding, it can cause error
    if new dataframe doesn't have enough unique elements as in the training
    dataframe
    """

    def __init__(self, column: str, drop_original_col: bool = True):
        self.column = column
        self.drop_original_col = drop_original_col
        # TODO: check if pandas can handle sparse = True
        self.onehot_encoder = preprocessing.OneHotEncoder(sparse=False)

    def fit_transform(
        self, data: pd.DataFrame, is_fit: bool = True
    ) -> pd.DataFrame:
        if is_fit:
            encoded_columns = self.onehot_encoder.fit_transform(
                data[[self.column]]
            )
        else:
            encoded_columns = self.onehot_encoder.transform(data[[self.column]])
        data = pd.concat(
            [
                data,
                pd.DataFrame(
                    encoded_columns,
                    data.index,
                    columns=[
                        self.column + "_" + str(i)
                        for i in range(encoded_columns.shape[1])
                    ],
                ),
            ],
            axis=1,
        )

        if self.drop_original_col:
            data = data.drop(columns=[self.column])
        return data

    def transform(self, data: pd.DataFrame) -> pd.DataFrame:
        return self.fit_transform(data, is_fit=False)


class SimpleImputer:
    def __init__(self, column: str, strategy: str):
        self.column = column
        self.imputer = impute.SimpleImputer(
            missing_values=np.nan, strategy=strategy
        )

    def fit_transform(self, data: pd.DataFrame) -> pd.DataFrame:
        assert self.column in data.columns
        data[self.column] = self.imputer.fit_transform(data[[self.column]])
        return data

    def transform(self, data: pd.DataFrame) -> pd.DataFrame:
        assert self.column in data.columns
        data[self.column] = self.imputer.transform(data[[self.column]])
        return data


def drop_columns(data: pd.DataFrame, threshold: float) -> pd.DataFrame:
    """Dropp columns with missing value rate higher than threshold.

    threshold: a float number between 0, 1
    """
    assert 0 < threshold < 1, threshold
    return data[data.columns[data.isnull().mean() < threshold]]


def drop_rows(data: pd.DataFrame, threshold: float) -> pd.DataFrame:
    """Drop rows with missing value rate higher than threshold.

    threshold: a float number between 0, 1
    """
    assert 0 < threshold < 1, threshold
    return data.loc[data.isnull().mean(axis=1) < threshold]


def fill_missing_by_zero(data: pd.DataFrame) -> pd.DataFrame:
    """Fill all missing values with 0."""
    return data.fillna(0)


def fill_missing_by_median(data: pd.DataFrame) -> pd.DataFrame:
    """Fill missing values with medians of the columns."""
    return data.fillna(data.median())


def fill_missing_categorical_by_most_common_(
    data: pd.DataFrame, column_name: str
) -> pd.DataFrame:
    """Fill missing categorical values by the most common one in the column."""
    assert (
        column_name in data.columns
    ), "column_name {} not in dataframe columns {}".format(
        column_name, data.columns
    )
    assert is_string_dtype(
        data[column_name]
    ), "{} contains non categorical data".format(data[column_name])
    data[column_name].fillna(
        data[column_name].value_counts().idxmax(), inplace=True
    )
