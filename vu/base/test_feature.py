import numpy as np
import pandas as pd
from qcore.asserts import assert_eq, AssertRaises

from vu.base import feature


def test_drop_columns():
    data = pd.DataFrame({"col1": [0, 1, 1], "col2": [np.nan, 0, np.nan]})
    got = feature.drop_columns(data, 0.5)
    expected = pd.DataFrame({"col1": [0, 1, 1]})
    expected.equals(got)


def test_drop_rows():
    data = pd.DataFrame({"col1": [0, 1, 1], "col2": [np.nan, 0, np.nan]})
    got = feature.drop_rows(data, 0.4)
    expected = pd.DataFrame({"col1": [1], "col2": [0]})
    expected.equals(got)


def test_fill_missing_by_zero():

    data = pd.DataFrame(
        {"col1": [0, 1, 1], "col2": [np.nan, 0, np.nan], "col3": [2, np.nan, 1]}
    )
    got = feature.fill_missing_by_zero(data)
    expected = pd.DataFrame(
        {"col1": [0, 1, 1], "col2": [0, 0, 0], "col3": [2, 0, 1]}
    )
    expected.equals(got)


def test_fill_missing_by_median():
    data = pd.DataFrame(
        {
            "col1": [0, 1, 1],
            "col2": [np.nan, 0, np.nan],
            "col3": [19, np.nan, 1],
        }
    )
    got = feature.fill_missing_by_median(data)
    expected = pd.DataFrame(
        {"col1": [0, 1, 1], "col2": [0, 0, 0], "col3": [999, 500, 1]}
    )
    expected.equals(got)


class TestFillMissingCategoricalByMostCommon:
    def test_categorical(self):
        data = pd.DataFrame(
            {
                "col1": [0, 1, 1, 2],
                "col2": [np.nan, 1, np.nan, np.nan],
                "col3": ["US", np.nan, "VN", "VN"],
            }
        )
        feature.fill_missing_categorical_by_most_common_(data, "col3")
        expected = pd.DataFrame(
            {
                "col1": [0, 1, 1, 2],
                "col2": [np.nan, "A", np.nan, np.nan],
                "col3": ["US", "VN", "VN", "VN"],
            }
        )
        expected.equals(data)

    def test_non_categorical(self):
        data = pd.DataFrame(
            {
                "col1": [0, 1, 1, 2],
                "col2": [np.nan, 1, np.nan, np.nan],
                "col3": ["US", np.nan, "VN", "VN"],
            }
        )

        with AssertRaises(AssertionError) as assert_raises:
            feature.fill_missing_categorical_by_most_common_(data, "col2")

        assertion_error = assert_raises.expected_exception_found
        assert_eq(
            True,
            assertion_error.args[0].endswith("contains non categorical data"),
        )
