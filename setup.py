import setuptools

setuptools.setup(
    name="kaggle_vu",
    version="0.0.1",
    author="TV",
    author_email="vuhuutiep@gmail.com",
    install_requires=[],
    packages=setuptools.find_packages(),
    package_data={"src": []},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
